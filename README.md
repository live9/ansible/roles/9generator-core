9generator-core
=========

Role with the core functionality to bootstrap a project using [9generator](https://gitlab.com/live9/ansible/playbooks/9generator).

This role is used to create the structure for a project that is going to be deployed using [9generator](https://gitlab.com/live9/ansible/playbooks/9generator). Currently these are the tasks it will perform:

* Crete output directories and subdirectories
* Render templates in jinja2 format and copy them to the output directories
* Copy any additional files needed for a project
* Fix permissions for scripts that are part of the project

Normally this role will be called from another role that is more specific to the project being bootstrapped and that probable does some other work. Take a look at the [9generator-jekyll](https://gitlab.com/live9/ansible/playbooks/9generator-jekyll) or [9generator-terraform](https://gitlab.com/live9/ansible/playbooks/9generator-terraform) roles as an example.


Requirements
------------

Python 3.

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

* _out_dir_: The output directory where this role should put the generated/copied fies. (Default value is _../_).
* _project_templates_: Directory where to find the templates to be rendered are stored. Normally the calling role will set this variable to its own _templates_ role path, so there is no need to put all needed templates on this role. (Default is _templates/{{ project_type }}_).
* _copy_files_path_: The path to the files or directories that need to be copied to _out_dir_. As with _project_templates_ it should be set by the calling role pointing to the path where the files can be found. (Default is _templates/_).
* _copy_directories_list_: A directory list of directories inside _copy_files_path_ that need to be copied. If this variable is not set from within the calling role then nothing will be copied. (Default is "").

Dependencies
------------

None.

Example Playbook
----------------

    - name: Bootstrap project
      hosts: localhost
      connection: local
      gather_facts: no
      vars:
        manifest_path: "manifest.yml.example"
        copy_files_path: "{{ role_path }}/templates/"
        copy_directories_list:
          - nginx
          - gitlab-ci

      tasks:
        - include_vars: "{{ manifest_path }}"
        - name: Set templates path
          set_fact:
            project_templates: "{{ role_path }}/templates/{{ project_type }}"
        - name: Bootstrap project
          include_role:
            name: 9generator-core
          tags:
            - 9generator_core

License
-------

GPLv3 or later

Author Information
------------------

Juan Luis Baptiste <juan.baptiste _at_ karisma.org.co >
